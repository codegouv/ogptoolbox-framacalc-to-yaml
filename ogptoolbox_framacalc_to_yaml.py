#! /usr/bin/env python3

# ogptoolbox-ethercal-to-yaml -- Convert OGP Toolbox Framacalc files to a Git repository of YAML files
# By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2015 Etalab
# https://git.framasoft.org/codegouv/ogptoolbox-ethercal-to-yaml
#
# ogptoolbox-ethercal-to-yaml is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# ogptoolbox-ethercal-to-yaml is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


import argparse
import codecs
import collections
import csv
import datetime
import logging
import os
import sys
import urllib.request

from slugify import slugify
import yaml


# YAML configuration


class folded_unicode(str):
    pass


class literal_unicode(str):
    pass


def dict_constructor(loader, node):
    return collections.OrderedDict(loader.construct_pairs(node))


def dict_representer(dumper, data):
    return dumper.represent_dict(sorted(data.items()))


yaml.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, dict_constructor)

yaml.add_representer(folded_unicode, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='>'))
yaml.add_representer(literal_unicode, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='|'))
yaml.add_representer(collections.OrderedDict, dict_representer)
yaml.add_representer(str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str', data))


#


app_name = os.path.splitext(os.path.basename(__file__))[0]
args = None
log = logging.getLogger(app_name)
short_labels = {
    "Catégorie (tags)": "Catégorie",
    "Code source (lien)": "URL code source",
    "Licence (choix dans une liste)": "Licence",
    "Nom complet de l'outil": "Nom",
    "Nom complet du projet": "Nom",
    "Nom du projet": "Nom",
    "Outil de suivi de bug (url)": "URL suivi de bogues",
    "Technologies utilisées (tags)": "Technologies",
    }


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('yaml_dir', help = 'path of target directory for generated YAML files')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
    global args
    args = parser.parse_args()
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    if not os.path.exists(args.yaml_dir):
        os.makedirs(args.yaml_dir)

    for spreadsheet_name, spreadsheet_url in (
            ('tools', 'https://framacalc.org/ogptoolbox.csv'),
            ('projects', 'https://framacalc.org/usages_ogptoolbox.csv'),
            ):
        spreadsheet_dir = os.path.join(args.yaml_dir, spreadsheet_name)
        if not os.path.exists(spreadsheet_dir):
            os.makedirs(spreadsheet_dir)

        response = urllib.request.urlopen(spreadsheet_url)
        reader = csv.reader(codecs.iterdecode(response, 'utf-8'))
        header = [
            cell.strip()
            for cell in next(reader)
            ]
        for row in reader:
            entry = collections.OrderedDict(
                (short_key, strip_value)
                for short_key, strip_value in (
                    (short_labels.get(key, key), value.strip()) 
                    for key, value in zip(header, row)
                    )
                if strip_value
                )
            if not entry:
                continue
            row_name = entry.get('Nom')
            if not row_name:
                print(spreadsheet_name, entry)
                continue
            slug = slugify(row_name)
            with open(os.path.join(spreadsheet_dir, '{}.yaml'.format(slug)), 'w') as yaml_file:
                yaml.dump(entry, yaml_file, allow_unicode = True, default_flow_style = False, indent = 2, width = 120)

    return 0


if __name__ == "__main__":
    sys.exit(main())
