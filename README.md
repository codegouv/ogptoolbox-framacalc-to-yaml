# ogptoolbox-framacalc-to-yaml

Convert OGP Toolbox Framacalc (Ethercalc-based) files to a Git repository of YAML files.

## Usage

```bash
./ogptoolbox_framacalc_to_yaml.py data/
```
